package com.devcam.s50.provincedistrictwardapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvinceDistrictWardApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProvinceDistrictWardApiApplication.class, args);
	}

}
