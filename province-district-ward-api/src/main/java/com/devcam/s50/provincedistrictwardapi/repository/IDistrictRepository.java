package com.devcam.s50.provincedistrictwardapi.repository;

import com.devcam.s50.provincedistrictwardapi.model.CDistrict;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IDistrictRepository extends JpaRepository<CDistrict,Integer>{
    CDistrict findById(int id);
    
}
