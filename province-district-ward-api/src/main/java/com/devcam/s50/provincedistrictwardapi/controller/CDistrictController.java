package com.devcam.s50.provincedistrictwardapi.controller;

import java.util.Set;

import com.devcam.s50.provincedistrictwardapi.model.CDistrict;

import com.devcam.s50.provincedistrictwardapi.model.CWard;
import com.devcam.s50.provincedistrictwardapi.repository.IDistrictRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDistrictController {
    @Autowired
    IDistrictRepository pIDistrictRepository;

    // load ra các Phường, Xa của 1 Quận, Huyen
    @GetMapping("/wards")
    public ResponseEntity <Set<CWard>> getWardList(@RequestParam(value="districtId")int district_id){
        try{
            CDistrict wardList = pIDistrictRepository.findById(district_id);

            if (wardList != null){
                return new ResponseEntity<>(wardList.getWards(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }     
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    
}
