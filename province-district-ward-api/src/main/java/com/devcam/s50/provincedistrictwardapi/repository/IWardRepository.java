package com.devcam.s50.provincedistrictwardapi.repository;

import com.devcam.s50.provincedistrictwardapi.model.CWard;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IWardRepository extends JpaRepository<CWard,Integer>{
    
}
