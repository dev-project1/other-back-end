package com.devcam.s50.provincedistrictwardapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.devcam.s50.provincedistrictwardapi.model.CDistrict;
import com.devcam.s50.provincedistrictwardapi.model.CProvince;
import com.devcam.s50.provincedistrictwardapi.repository.IProvinceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CProvinceController {
    @Autowired
    IProvinceRepository pIProvinceRepository;

    // load ra toàn bộ các Tinh, Thành phố 
    @GetMapping("/provinces")
    public ResponseEntity <List<CProvince>> getAllProvince(){
        try{
            List<CProvince> allProvince =new ArrayList<>();

            pIProvinceRepository.findAll().forEach(allProvince::add);

            return new ResponseEntity<>(allProvince, HttpStatus.OK);
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    // load ra các Quận, Huyen của 1 Tinh 
    @GetMapping("/districts")
    public ResponseEntity <Set<CDistrict>> getDistrictList(@RequestParam(value="provinceId")int province_id){
        try{
            CProvince districtList = pIProvinceRepository.findById(province_id);

            if (districtList != null){
                return new ResponseEntity<>(districtList.getDistricts(), HttpStatus.OK);
            } else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }     
        } catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    
}
