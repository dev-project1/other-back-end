package com.devcam.s50.provincedistrictwardapi.repository;

import com.devcam.s50.provincedistrictwardapi.model.CProvince;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IProvinceRepository extends JpaRepository<CProvince,Integer>{
    CProvince findById(int id);
    
}
