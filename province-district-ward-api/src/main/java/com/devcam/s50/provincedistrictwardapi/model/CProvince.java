package com.devcam.s50.provincedistrictwardapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
@Entity
@Table(name="provinces")
public class CProvince {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name="name")
    private String name;
    @Column(name="code")
    private String code;
    @OneToMany(mappedBy = "province",cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CDistrict> districts;
    public CProvince() {
    }
    public CProvince(String name, String code) {
        this.name = name;
        this.code = code;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public Set<CDistrict> getDistricts() {
        return districts;
    }
    public void setDistricts(Set<CDistrict> districts) {
        this.districts = districts;
    }
    
    
}
